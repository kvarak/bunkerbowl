#!/bin/bash

sheets=( 1c_ksQ5azUB0YsW2u_uait0Aty4crIR6qy1dSMVXoxSU \
         1BY2FAPiFnEqDB84bnnga4nSfoeN7R7AHWQjFi2xGEYI \
         . \
         1qwYVySOod7C1JfXmNWytCkkI9IQQS2M6u25epmOsPzs )

csvfiles=( games \
           data \
           playerdata )

loc="data"
rm -rf $loc
mkdir $loc

season=1

echo "" > tmp

for sheetid in "${sheets[@]}"
do

  curl "https://docs.google.com/spreadsheets/d/$sheetid/export" -o $loc/data.xlsx

  if [ "$sheetid" != "." ] ; then
    xlsx2csv -a -i --skipemptycolumns -e $loc/data.xlsx $loc/$season

    targetloc=jekyll/_data/s$season
    mkdir -p $targetloc

    imgloc=jekyll/assets/img
    mkdir -p $imgloc

    for csvf in "${csvfiles[@]}"
    do

      linenr=0
      while read line
      do

        maybefile=$(echo $line | sed 's/.*http/http/' | sed 's/,.*//' | grep http)
        if [ "$maybefile" != "" ] ; then
          echo $maybefile >> tmp
          newfilename=$(echo $line | sed 's/[^,]*,//' | sed 's/[^,]*,//' | sed 's/,.*//' | sed -e 's/[^A-Za-z0-9._-]/_/g' | sed 's/^_*//' )
          newfileending=$(echo $maybefile | sed 's/.*\.//')
          curl $maybefile -o $loc/$season/$newfilename.$newfileending
          echo "$loc/$season/$newfilename.$newfileending"
          mogrify -thumbnail 128x128 -format png -path $imgloc $loc/$season/$newfilename.$newfileending[0]
          echo $line | sed "s*$maybefile*$newfilename.png*" >> $targetloc/$csvf.csv
        elif [ $linenr == 0 ] ; then
          echo $line | tr ' ' '_' | tr '#' 'x' | tr '<' '_' | tr '>' '_' > $targetloc/$csvf.csv
        else
          echo $line | sed 's/\.0,/,/g' >> $targetloc/$csvf.csv
        fi
        linenr=$((linenr+1))
      done < $loc/$season/$csvf.csv

    done

  fi

  season=$((season+1))

done

cat tmp
